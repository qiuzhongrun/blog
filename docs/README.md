---
home: true
heroImage: /hero.gif
heroImageStyle: {
  maxWidth: '600px',
  width: '100%',
  display: block,
  margin: '9rem auto 2rem',
  background: '#fff',
  borderRadius: '1rem',
}
isShowTitleInHome: false
actionText: ~ oh ! fresh meat ~
actionLink: /timeLine/
features:
- title: 简单易懂
  details: 最大化拉低我的智商水平以便和您的接轨，如果还读不懂，那我只能任由你骂我了。正所谓常与智者争天下,不与智者论长短。
- title: 匠心独运
  details: 尽可能地假装我都懂，以便能忽悠您让您觉得自个也懂了。连自己都骗不了的人，算不上狼人。
- title: 灵感碰撞
  details: 我会尽可能无规律地写，您就随意扒拉着看。金属的碰撞最能激发灵感，支付宝微信都可以。
---

### 程序员快速拥有对象的5种有效办法

``` java
// 你有一个叫冬梅的空对象
Object dongMei;

// ①你可以直接new一个
dongMei = new Object();

// ②你可以用Class的newInstance调用无参的构造函数创建一个
dongMei = Obejct.class.newInstance();

// ③你也可以调用一个有参的或者私有的构造函数创建一个，这也是各大框架喜欢的方式，比如春天系列框架
Constructor<Obejct> constructor = Obejct.class.getConstructor();
dongMei = constructor.newInstance();

// ④如果你已经有了心仪的对象，不防克隆一个
dongMei = (Obejct) dongMei.clone();

// ⑤当然，我们也很鼓励进口一些优质对象对冲一下
ObjectInputStream in = new ObjectInputStream(new FileInputStream("Ukraine.obj"));
dongMei = (Obejct) in.readObject();

```


::: danger FBI WARNING
本站 纯属虚构 如有雷同 算我抄袭
:::