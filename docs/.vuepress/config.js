module.exports = {
  title: "Qiu Zhongrun",
  description: '吉他毁一生 Java穷三代 若为自由故 两者皆可抛',
  dest: 'public',
  head: [
    ['link', { rel: 'icon', href: '/favicon.ico' }],
    ['meta', { name: 'viewport', content: 'width=device-width,initial-scale=1,user-scalable=no' }]
  ],
  theme: 'reco',
  themeConfig: {
    nav: [
      { text: 'Home', link: '/', icon: 'reco-home' },
      { text: 'Latest blogs', link: '/timeLine/', icon: 'reco-date' },
      { text: 'About', 
        icon: 'reco-message',
        items: [
          { text: 'GitLab', link: 'https://gitlab.com/qiuzhongrun', icon: 'reco-other' },
          { text: 'GitHub', link: 'https://github.com/qiuzhongrun', icon: 'reco-github' },
          { text: 'about', link: '/views/other/about', icon: 'reco-account' },
        ]
      }
    ],
    // 博客设置
    blogConfig: {
      category: {
        location: 3, // 在导航栏菜单中所占的位置，默认2
        text: 'Category' // 默认 “分类”
      },
      tag: {
        location: 4, // 在导航栏菜单中所占的位置，默认3
        text: 'Tag' // 默认 “标签”
      }
    },
    logo: '/head.png',
    // 搜索设置
    search: true,
    searchMaxSuggestions: 10,
    // 自动形成侧边导航
    sidebar: 'auto',
    // 最后更新时间
    lastUpdated: 'Last Updated',
    // 作者
    author: 'Qiu Zhongrun',
    // 备案号
    record: '5163406',
    // 项目开始时间
    startYear: '2019'
    /**
     * 密钥 (if your blog is private)
     */

    // keyPage: {
    //   keys: ['your password'],
    //   color: '#42b983',
    //   lineColor: '#42b983'
    // },

    /**
     * valine 设置 (if you need valine comment )
     */

    // valineConfig: {
    //   appId: '...',// your appId
    //   appKey: '...', // your appKey
    // }
  },
  markdown: {
    lineNumbers: true
  },
  plugins: ['@vuepress/medium-zoom', 'flowchart']
}  