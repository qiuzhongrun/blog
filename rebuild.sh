#!/bin/sh
function log()
{
    local curtime=`date "+%Y-%m-%d %H:%M:%S"`
    echo "$curtime $*">> rebuild.log
}

cd `dirname $0`

. /etc/profile
. ~/.bash_profile

gpop=`git pull`

log $gpop